<?php $pagename = "contact";?>
<?php include('header.php');?>
<section class="no-margin">
	<iframe width="100%" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d904.7787697471474!2d91.86554136076164!3d24.894055979530954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjTCsDUzJzM4LjUiTiA5McKwNTEnNTQuOSJF!5e0!3m2!1sen!2s!4v1392707569222"></iframe>
</section>

<section id="contact-page" class="container">
	<div class="row-fluid">

		<div class="span8">
			<h4>Contact Form</h4>
			<div class="status alert alert-success" style="display: none"></div>

			<form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
				<div class="row-fluid">
					<div class="span5">
						<label>First Name</label>
						<input type="text" class="input-block-level" name="name1" required="required" placeholder="Your First Name">
						<label>Last Name</label>
						<input type="text" class="input-block-level" name="name2" required="required" placeholder="Your Last Name">
						<label>Email Address</label>
						<input type="text" class="input-block-level" name="email" required="required" placeholder="Your email address">
					</div>
					<div class="span7">
						<label>Message</label>
						<textarea name="message" id="message" required="required" class="input-block-level" rows="8"></textarea>
					</div>

				</div>
				<button type="submit" class="btn btn-primary btn-large pull-right">Send Message</button>
				<p> </p>

			</form>
		</div>

		<div class="span3">
			<h4>Our Address</h4>
			<p>You can meet us at our office. Office is open from 10am to 6pm everyday.</p>
			<p>
				<i class="icon-map-marker pull-left"></i> 3rd Floor, Tanim Tower, Jallarpar Road<br>
				Sylhet-3100
			</p>
			<p>
				<i class="icon-envelope"></i> &nbsp;admin@functioneducare.com
			</p>
			<p>
				<i class="icon-phone"></i> &nbsp;01793510220, 01793510225
			</p>
			<p>
				<i class="icon-globe"></i> &nbsp;http://www.functioneducare.com
			</p>
		</div>

	</div>

</section>
<?php include('footer.php');?>