<section id="bottom" class="main">
    <!--Container-->
    <div class="container">

        <!--row-fluids-->
        <div class="row-fluid">

            <!--Contact Form-->
            <div class="span3">
                <h4>ADDRESS</h4>
                <ul class="unstyled address">
                    <li>
                        <i class="icon-home"></i><strong>Address:</strong> Tanim Tower, Jallarpar Road<br>Sylhet 3100
                    </li>
                    <li>
                        <i class="icon-envelope"></i>
                        <strong>Email: </strong> admin@functioneducare.com
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <strong>Website:</strong> www.functioneducare.com
                    </li>
                    <li>
                        <i class="icon-phone"></i>
                        <strong>Phone:</strong> 01793510220
                    </li>
                </ul>
            </div>
            <!--End Contact Form-->

            <!--Important Links-->
            <div id="tweets" class="span3">
                <h4>Pages</h4>
                <div>
                    <ul class="arrow">
                        <li><a href="about-us.php">About Us</a></li>
                        <li><a href="admission-care.php">Admission Care Program</a></li>
                        <li><a href="#">Robotics Workshop</a></li>
                        <li><a href="#">Programming Classes</a></li>
                        <li><a href="gallery.php">Photo Gallery</a></li>
                        <li><a href="contact-us.php">Contact Us</a></li>
                    </ul>
                </div>  
            </div>
            <!--Important Links-->

            <!--Archives-->
            <div id="archives" class="span3">
                <h4>ARCHIVES</h4>
                <div>
                    <ul class="arrow">
                        <li><a href="#">December 2012 (0)</a></li>
                        <li><a href="#">November 2012 (0)</a></li>
                        <li><a href="#">October 2012 (0)</a></li>
                        <li><a href="#">September 2012 (0)</a></li>
                        <li><a href="#">August 2012 (0)</a></li>
                        <li><a href="#">July 2012 (0)</a></li>
                    </ul>
                </div>
            </div>
            <!--End Archives-->

            <div class="span3">
                <h4>PHOTO GALLERY</h4>
                <div class="row-fluid first">
                    <ul class="thumbnails">
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (1).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (1).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (2).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (2).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (3).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (3).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (4).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (4).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                    </ul>
                </div>
                <div class="row-fluid">
                    <ul class="thumbnails">
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (5).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (5).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (6).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (6).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (7).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (7).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                        <li class="span3">
                            <a href="images/gallery/function-basic-challenge (8).jpg" title="Function Basic Challenge"><img src="images/gallery/function-basic-challenge (8).jpg" width="75" height="75" alt="Function Basic Challenge"></a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
        <!--/row-fluid-->
    </div>
    <!--/container-->

</section>
<!--/bottom-->
<!--Footer-->
<footer id="footer">
    <div class="container">
        <div class="row-fluid">
            <div class="span5 cp">
                &copy; 2014 <a target="_blank" href="http://functioneducare.com/" title="Admission and academic care for sylhet students">Function Education Care</a>. All Rights Reserved.
            </div>
            <!--/Copyright-->

            <div class="span6">
                <ul class="social pull-right">
                    <li><a href="https://www.facebook.com/functioneducare"><i class="icon-facebook"></i></a></li>
                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                    <li><a href="#"><i class="icon-pinterest"></i></a></li>
                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                    <li><a href="#"><i class="icon-google-plus"></i></a></li>                       
                    <li><a href="#"><i class="icon-youtube"></i></a></li>
                    <li><a href="#"><i class="icon-tumblr"></i></a></li>                        
                    <li><a href="#"><i class="icon-dribbble"></i></a></li>
                    <li><a href="#"><i class="icon-rss"></i></a></li>
                    <li><a href="#"><i class="icon-github-alt"></i></a></li>
                    <li><a href="#"><i class="icon-instagram"></i></a></li>                   
                </ul>
            </div>

            <div class="span1">
                <a id="gototop" class="gototop pull-right" href="#"><i class="icon-angle-up"></i></a>
            </div>
            <!--/Goto Top-->
        </div>
    </div>
</footer>
<!--/Footer-->

<!--  Login form -->
<div class="modal hide fade in" id="loginForm" aria-hidden="false">
    <div class="modal-header">
        <i class="icon-remove" data-dismiss="modal" aria-hidden="true"></i>
        <h4>Login Form</h4>
    </div>
    <!--Modal Body-->
    <div class="modal-body">
        <form class="form-inline" action="index.html" method="post" id="form-login">
            <input type="text" class="input-small" placeholder="Email">
            <input type="password" class="input-small" placeholder="Password">
            <label class="checkbox">
                <input type="checkbox"> Remember me
            </label>
            <button type="submit" class="btn btn-primary">Sign in</button>
        </form>
        <a href="#">Forgot your password?</a>
    </div>
    <!--/Modal Body-->
</div>
<!--  /Login form -->

<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- Required javascript files for Slider -->
<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>
<!-- /Required javascript files for Slider -->

<!-- SL Slider -->
<script type="text/javascript"> 
$(function() {
    var Page = (function() {

        var $navArrows = $( '#nav-arrows' ),
        slitslider = $( '#slider' ).slitslider( {
            autoplay : true
        } ),

        init = function() {
            initEvents();
        },
        initEvents = function() {
            $navArrows.children( ':last' ).on( 'click', function() {
                slitslider.next();
                return false;
            });

            $navArrows.children( ':first' ).on( 'click', function() {
                slitslider.previous();
                return false;
            });
        };

        return { init : init };

    })();

    Page.init();
});
</script>
<!-- /SL Slider -->
</body>
</html>