<?php $pagename = "home";?>
<?php include('header.php');?>

<!--Slider-->
<section id="slide-show">
	<div id="slider" class="sl-slider-wrapper">

		<!--Slider Items-->    
		<div class="sl-slider">
			<!--Slider Item1-->
			<div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
				<div class="sl-slide-inner">
					<div class="container">
						<img class="pull-right" src="images/sample/slider/slide1.png" alt="" />
						<h2>Admission Care Program</h2>
						<h3 class="gap">This program is aimed to help sylheti students be better prepared for BUET and other university admission tests</h3>
						<a class="btn btn-large btn-transparent" href="admission-care.php">Learn More</a>
					</div>
				</div>
			</div>
			<!--/Slider Item1-->

			<!--Slider Item2-->
			<div class="sl-slide item2" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
				<div class="sl-slide-inner">
					<div class="container">
						<img class="pull-right" src="images/sample/slider/img2.png" alt="" />
						<h2>Basic Robotics Workshop</h2>
						<h3 class="gap">We are going to arrange workshop on basic robotics. Details of this program is not fixed yet.</h3>
						<a class="btn btn-large btn-transparent" href="javascript:void();">Coming Soon</a>
					</div>
				</div>
			</div>
			<!--Slider Item2-->

			<!--Slider Item3-->
			<div class="sl-slide item3" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
				<div class="sl-slide-inner">
					<div class="container">
						<img class="pull-right" src="images/sample/slider/img3.png" alt="" />
						<h2>Programming Classes</h2>
						<h3 class="gap">We are going to arrange programming classes for college and school students. Details of this program is not fixed yet.</h3>
						<a class="btn btn-large btn-transparent" href="javascript:void();">Coming Soon</a>
					</div>
				</div>
			</div>
			<!--Slider Item3-->

		</div>
		<!--/Slider Items-->

		<!--Slider Next Prev button-->
		<nav id="nav-arrows" class="nav-arrows">
			<span class="nav-arrow-prev"><i class="icon-angle-left"></i></span>
			<span class="nav-arrow-next"><i class="icon-angle-right"></i></span> 
		</nav>
		<!--/Slider Next Prev button-->

	</div>
	<!-- /slider-wrapper -->           
</section>
<!--/Slider-->
<section class="main-info">
	<div class="container">
		<div class="row-fluid">
			<div class="span9">
				<h4>Function Education Care</h4>
				<p class="no-margin">3rd Floor, Tanim Tower, Jallarpar Road, Sylhet</p>
			</div>
			<div class="span3">
				<a class="btn btn-success btn-large pull-right gotomap" href="#ourmap">Find Office Location On Map</a>
			</div>
		</div>
	</div>
</section>

<section id="recent-works">
	<div class="container">
		<div class="center">
			<h3>Function Basic Challenge- 2014</h3>
			<p class="lead">Here are some photographs of Function Basic Challenge 2014</p>
		</div>  
		<div class="gap"></div>
		<ul class="gallery col-4">
			<!--Item 1-->
			<li>
				<div class="preview">
					<img alt=" " src="images/gallery/fbc-15-3.jpg">
					<div class="overlay">
					</div>
					<div class="links">
						<a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
					</div>
				</div>
				<div class="desc">
					<h5>Winners Of Function Basic Challenge- 2014 with directors</h5>
				</div>
				<div id="modal-1" class="modal hide fade">
					<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
					<div class="modal-body">
						<img src="images/gallery/fbc-15-3.jpg" alt=" " width="100%" style="max-height:400px">
					</div>
				</div>                 
			</li>
			<!--/Item 1-->          
			<!--Item 2-->
			<li>
				<div class="preview">
					<img alt=" " src="images/gallery/fbc-15-2.jpg">
					<div class="overlay">
					</div>
					<div class="links">
						<a data-toggle="modal" href="#modal-2"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
					</div>
				</div>
				<div class="desc">
					<h5>Students in exam hall</h5>
				</div>
				<div id="modal-2" class="modal hide fade">
					<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
					<div class="modal-body">
						<img src="images/gallery/fbc-15-2.jpg" alt=" " width="100%" style="max-height:400px">
					</div>
				</div>                 
			</li>
			<!--/Item 2-->
			<!--Item 3-->
			<li>
				<div class="preview">
					<img alt=" " src="images/gallery/fbc-15-1.jpg">
					<div class="overlay">
					</div>
					<div class="links">
						<a data-toggle="modal" href="#modal-3"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
					</div>
				</div>
				<div class="desc">
					<h5>Champion Of Function Basic Challenge- 2014</h5>
				</div>
				<div id="modal-3" class="modal hide fade">
					<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
					<div class="modal-body">
						<img src="images/gallery/fbc-15-1.jpg" alt=" " width="100%" style="max-height:400px">
					</div>
				</div>                 
			</li>
			<!--/Item 3-->
			<!--Item 4-->
			<li>
				<div class="preview">
					<img alt=" " src="images/gallery/fbc-15-5.jpg">
					<div class="overlay">
					</div>
					<div class="links">
						<a data-toggle="modal" href="#modal-4"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
					</div>
				</div>
				<div class="desc">
					<h5>Presentation on robotics</h5>
				</div>
				<div id="modal-4" class="modal hide fade">
					<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
					<div class="modal-body">
						<img src="images/gallery/fbc-15-5.jpg" alt=" " width="100%" style="max-height:400px">
					</div>
				</div>                 
			</li>
			<!--/Item 4-->
		</ul>
	</div>

</section>

<section id="about-us" class="main">
	<div class="container">
		<hr>
		<?php include('team.php');?>
	</div>


</section>
<section id="ourmap" class="no-margin">
        <iframe width="100%" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d904.7787697471474!2d91.86554136076164!3d24.894055979530954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjTCsDUzJzM4LjUiTiA5McKwNTEnNTQuOSJF!5e0!3m2!1sen!2s!4v1392707569222"></iframe>
</section>
<!--Bottom-->

<?php include('footer.php');?>