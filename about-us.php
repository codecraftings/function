﻿<?php $pagename = "about";?>
<?php include('header.php');?>
<section class="title">
    <div class="container">
        <div class="row-fluid">
            <div class="span6">
                <h1>About Us</h1>
            </div>
            <div class="span6">
                <ul class="breadcrumb pull-right">
                    <li><a href="index.php">Home</a> <span class="divider">/</span></li>
                    <li class="active">About Us</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- / .title -->   

<section id="about-us" class="container main">
    <div class="row-fluid">
        <div class="span13">
            <h1>OUR MISSION</h1>
            <p class="big-text">
                => Getting tensed for admission test of BUET, DU, SUST?<br>
=> Are you an HSC(Science) examinee or an admission test examinee?<br>
=> Facing problems with Mathematics, Physics, Chemistry, ICT?<br>
=> Can't find a suitable solution to your problem?<br>
            </p>
            <p class="big-text">We're here to help you solve your problems. A few of us Sylheti BUETian students have opened this FUNCTION group to help you. We know that there are a lot of talents in Sylhet. And that studying in BUET and DU is a lifetime dream of many students. But, maybe due to reluctance and lack of proper guidance, number of students admitted to BUET and DU from Sylhet isn't satisfactory. We want more talents from Sylhet to shine. So, we'll work for enhancing your confidence level and developing your basic knowledge. You can come to us regarding a critical problem related to your academic education or related to your admission studies. We'll try our level best to make your problem easy and help you with the proper guidelines to solve your problem. You can also prove your ability by solving critical problems related to your studies. Remember, if you have the will, the determination, the hard work and the proper guideline, you can reach your destination and we're here to help you fulfill your dreams. Wish you all a successful academic future.</p> <br>
            <h1>OUR FEATURES</h1>
            <p class="big-text">
                 নান্দনিক প্রাকৃতিক সৌন্দর্যে ভরপুর আমাদের ঐতিহ্যবাহী সিলেট। কিন্তু আক্ষেপের বিষয় বৃহত্তর সিলেট অঞ্চল থেকে বাংলাদেশ প্রকৌশল বিশ্ববিদ্যালয়, ঢাকা বিশ্ববিদ্যালয় সহ দেশের অন্যান্য ভালো বিশ্ববিদ্যালয়ে শিক্ষার্থীর সংখ্যা খুবই কম। সিলেটে অনেক মেধাবী শিক্ষার্থী থাকা সত্ত্বেও আত্মবিশ্বাস ও সঠিক দিক নির্দেশনার অভাবে অনেকেই কাঙ্খিত প্রতিষ্ঠানেই চান্স পায় না। তাই সিলেট অঞ্চলের বিশ্ববিদ্যালয়ে ভর্তিচ্ছু শিক্ষার্থীদের আত্মবিশ্বাস বৃদ্ধি ও সঠিক দিক নির্দেশনা প্রদান করার লক্ষে ফাংশন এডুকেশন কেয়ার এর যাত্রা শুরু। আমাদের সীমিত সংখ্যক ছাত্রছাত্রীদের নিয়ে গঠিত ক্লাসে বুয়েট, ঢাবি, সাস্ট, মেডিকেল এর অভিজ্ঞ ছাত্রছাত্রীদের দ্বারা নেয়া ক্লাস গুলো শিক্ষার্থীদের জন্য অত্যন্ত সহায়ক হবে বলে আশা করি। শুধু বিশ্ববিদ্যালয়ে চান্স পাইয়ে দেয়াই আমাদের উদ্দেশ্য না বরং শিক্ষার্থীদের বেসিক জ্ঞান তৈরি করে দেয়াই আমাদের লক্ষ্য।
</p>
<h3>আমাদের মতে সিলেটের শিক্ষার্থীদের ভর্তি পরীক্ষায় ব্যর্থতার মূল কারণঃ</h3>
<p class="big-text">
* শিক্ষার্থীদের মেধা থাকা সত্ত্বেও চান্স পাবে এই কনফিডেন্স না থাকা।<br> <br>
* সঠিক দিক নির্দেশনা না পাওয়া।<br> <br>
* গতানুগতিক বাণিজ্যিক কোচিং গুলার বিশাল আকারের ক্লাস। ( কোন কোন জায়গায় ৭০-৮০ জন স্টুডেন্ট নিয়ে ক্লাস গঠন করা হয়!!!)<br> <br>
* সাধারন কোচিং গুলোতে গতানুগতিক উপায়ে কঠিন করে পড়ানো। ( যার ফলাফল কোচিং গুলার সফলতার হার ০-৩% এই সীমাবদ্ধ )<br> <br>
* কনসেপ্ট ভিত্তিক পড়াশুনা না করানো।<br> <br>
* বিভিন্ন ইউনিভার্সিটির ভর্তি পরীক্ষার প্রশ্ন সম্পর্কে পরিষ্কার ধারনা না থাকা।
</p> <br>
<h3>আমাদের কিছু ব্যতিক্রম ধর্মী বৈশিষ্ট্য:</h3>
<p class="big-text">
* শুধুমাত্র বুয়েটে অধ্যয়নরত কিছু সংখ্যক ছাত্রদের দ্বারা পরিচালিত সিলেটের একমাত্র প্রতিষ্ঠান যেখানে প্রকৌশল বিশ্ববিদ্যালয় সহ অন্যান্য বিশ্ববিদ্যালয়ের ভর্তি পরীক্ষার জন্য সঠিক দিক নির্দেশনা প্রদান করা হয়।<br> <br>
* English Version ও English Medium এর ছাত্রছাত্রীদের জন্য সিলেটে এই প্রথম বারের মত আলাদা ক্লাস এবং আলাদা লেকচার শিটের মাধ্যমে পাঠদানের ব্যবস্থা।<br> <br>
* ইঞ্জিনিয়ারিং সেকশন সমূহের প্রতিটা ক্লাসে সরাসরি বুয়েট থেকে আগত ছাত্রদের দ্বারা পাঠদান।<br> <br>
* ইউনিভার্সিটি সেকশনের সর্বমোট ক্লাসের অন্তত ৫০% ক্লাস বুয়েট থেকে আগত ছাত্রদের দ্বারা পাঠদান করার ব্যবস্থা । বাকি ক্লাস গুলো ঢাকা বিশ্ববিদ্যালয়, শাহজালাল বিশ্ববিদ্যালয় ও মেডিকেলের মেধাবী শিক্ষার্থীদের দ্বারা নেয়া হবে।<br> <br>
* ক্লাসের ভালো মান বজায় রাখার জন্য সীমিত সংখ্যক শিক্ষার্থী নিয়ে ক্লাস গঠন।<br> <br>
* শিক্ষার্থীদের প্রয়োজন অনুযায়ী সর্বোচ্চ সল্ভ ক্লাসের ব্যবস্থা।<br> <br>
* শাহজালাল বিশ্ববিদ্যালয়, ঢাকা বিশ্ববিদ্যালয় সহ অন্যান্য বিশ্ববিদ্যালয়ের জন্য সর্বোচ্চ সংখ্যক স্পেশাল মডেল টেস্ট নেয়ার ব্যবস্থা।<br> <br>
* প্রতিদিনের ক্লাস টেস্ট সহ বাকি সকল পরীক্ষার ফলাফল ওয়েবসাইটে এবং এসএমএসের মাধ্যমে প্রদানের ব্যবস্থা।<br> <br>
* বিভিন্ন ইউনিভার্সিটির সম্পর্কে শিক্ষার্থীদের ধারনা দেয়ার জন্য সেমিনারের আয়োজন।<br>
            </p>
        </div>
    </div>

    <hr>

    <?php include('team.php');?>
</section>
<?php include('footer.php');?>