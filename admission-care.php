<?php $pagename = "admission";?>
<?php include('header.php');?>
<section class="title">
	<div class="container">
		<div class="row-fluid">
			<div class="span6">
				<h1>Admission Care Program</h1>
			</div>
			<div class="span6">
				<ul class="breadcrumb pull-right">
					<li><a href="index.php">Home</a> <span class="divider">/</span></li>
					<li class="active">Admission Care Program</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="programme-intor" class="main gray-background">
	<div class="container">
		<p class="big-text">
		We know that there are a lot of talented students in Sylhet. And that studying in BUET, DU, SUST and other public universities is a lifetime dream of many students. But, maybe due to reluctance and lack of proper guidance, number of students admitted to BUET, DU, SUST from Sylhet isn't satisfactory. We want more talents from Sylhet to shine. So, our Engineering and University admission programs are aimed to work for enhancing confidence level and developing basic knowledge of students.
		</p>
	</div>
</section>
<section id="programme-intor" class="main blue-background">
	<div class="container">
		<h2>What are the course plan and packages?</h2>
		<p>
		<img src="images/packages.png" />
		</p>
	</div>
</section>
<section id="programme-intor" class="main white-background">
	<div class="container">
		<h2>How to register for the classes?</h2>
		<p class="big-text">
		The process is pretty straight forward. If you come to our office (Tanim Tower, Jollarpar Road), our manager will explain everything to you. You just need to have 2 copies of your passport size photograph. Then just fill out the admission form which you can collect from our <a href="contact-us.php">office</a>.
		</p>
	</div>
</section>
<section id="programme-intor" class="main gray-background">
	<div class="container">
		<h2>What is the registration and class timeline?</h2>
		<p class="big-text">
		Registration is open from 21st february. However, the exact date for classes is not fixed yet. It will be decided after reviewing the complete schedule of HSC exam. Hopefully it will be between first or second week of June. 
		<br> 
		<h3>If you register before 15th May, you will get 25% discount on all packages!</h3>
		<h3><a href="https://www.facebook.com/events/242195635942863">FUNCTION BASIC CHALLENGE 2013</a> participants will get 15% discount on all packages!</h3>
		<h3>If 5 of you register together, you get 20% discount!</h3>
		</p>
	</div>
</section>
<?php include('footer.php');?>