<?php $pagename = "gallery";?>
<?php include('header.php');?>
<section class="title">
	<div class="container">
		<div class="row-fluid">
			<div class="span6">
				<h1>Photo Gallery</h1>
			</div>
			<div class="span6">
				<ul class="breadcrumb pull-right">
					<li><a href="index.php">Home</a> <span class="divider">/</span></li>
					<li class="active">Gallery</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section id="portfolio" class="container main">    
	<ul class="gallery col-4">
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/fbc-15-3.jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Winners Of Function Basic Challenge- 2013 with directors</h5>
			</div>
			<div id="modal-1" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/fbc-15-3.jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/fbc-15-2.jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Moments of Function Basic Challenge 2014</h5>
			</div>
			<div id="modal-1" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/fbc-15-2.jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/fbc-15-1.jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Moments of Function Basic Challenge 2014</h5>
			</div>
			<div id="modal-1" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/fbc-15-1.jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/fbc-15-5.jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Moments of Function Basic Challenge 2014</h5>
			</div>
			<div id="modal-1" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/fbc-15-5.jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--Item 1-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (1).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Winners Of Function Basic Challenge- 2013 with directors</h5>
			</div>
			<div id="modal-1" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (1).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 1-->          
		<!--Item 2-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (2).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-2"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Function Basic Challenge 2013</h5>
			</div>
			<div id="modal-2" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (2).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 2-->
		<!--Item 3-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (3).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-3"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Function Basic Challenge 2013</h5>
			</div>
			<div id="modal-3" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (3).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 3-->
		<!--Item 4-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (4).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-4"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Function Basic Challenge 2013</h5>
			</div>
			<div id="modal-4" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (4).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 4-->
		<!--Item 5-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (5).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-5"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Function Basic Challenge 2013</h5>
			</div>
			<div id="modal-5" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (5).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 5-->
		<!--Item 6-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (6).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-6"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Function Basic Challenge 2013</h5>
			</div>
			<div id="modal-6" class="modal hide fade">
				<a class="close-modal" href="javascript:void();" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (6).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>
		</li>
		<!--/Item 6-->
		<!--Item 7-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (7).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-7"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Function Basic Challenge 2013</h5>
			</div>
			<div id="modal-7" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (7).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 7-->
		<!--Item 8-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (8).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-8"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Function Basic Challenge 2013</h5>
			</div>
			<div id="modal-8" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (8).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 8-->
		<!--Item 9-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (9).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-9"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Winners of Function Basic Challenge 2013 with directors</h5>
			</div>
			<div id="modal-9" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (9).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 9-->
		<!--Item 10-->
		<li>
			<div class="preview">
				<img alt=" " src="images/gallery/function-basic-challenge (10).jpg">
				<div class="overlay">
				</div>
				<div class="links">
					<a data-toggle="modal" href="#modal-10"><i class="icon-eye-open"></i></a><a href="#"><i class="icon-link"></i></a>                          
				</div>
			</div>
			<div class="desc">
				<h5>Seminar on Engineering and University Admission</h5>
			</div>
			<div id="modal-10" class="modal hide fade">
				<a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
				<div class="modal-body">
					<img src="images/gallery/function-basic-challenge (10).jpg" alt=" " width="100%" style="max-height:400px">
				</div>
			</div>                 
		</li>
		<!--/Item 10-->
	</ul>
</section>
<?php include('footer.php');?>