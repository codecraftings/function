<!-- Meet the team -->
    <h1 class="center">Meet The Team</h1>

    <hr>

    <div class="row-fluid">
        <div class="row-fluid">
            <div class="span3">
                <div class="box director-home">
                    <div class="pull-left span3"><p><img src="https://graph.facebook.com/osman.goni.39/picture?type=square" alt="Osman Goni" ></p></div>
                    <div class="pull-right span8">
                        <h5><a href="https://www.facebook.com/osman.goni.39">Osman Goni</a></h5>
                        <p>Chemical Engineering<br> <a href="http://www.buet.ac.bd/">BUET</a><br>
                            Mobile: 01715039224</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="span3">
            <div class="box director-home">
                <div class="pull-left span3"><p><img src="https://graph.facebook.com/abc.asif/picture?type=square" alt="ABC Asif" ></p></div>
                <div class="pull-right span8">
                    <h5><a href="https://www.facebook.com/abc.asif">AB Chowdhury Asif</a></h5>
                    <p>Chemical Engineering<br> <a href="http://www.buet.ac.bd/">BUET</a><br>
                        Mobile: 01750358348</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="span3">
            <div class="box director-home">
                <div class="pull-left span3"><p><img src="https://graph.facebook.com/abu.abdullahshourav/picture?type=square" alt="Shourav" ></p></div>
                <div class="pull-right span8">
                    <h5><a href="https://www.facebook.com/abu.abdullahshourav">Abdullah Shourav</a></h5>
                    <p>Mechanical Engineering<br> <a href="http://www.buet.ac.bd/">BUET</a><br>
                        Mobile: 01911319547</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="span3">
            <div class="box director-home">
                <div class="pull-left span3"><p><img src="https://graph.facebook.com/dozerabbasi/picture?type=square" alt="Zahid" ></p></div>
                <div class="pull-right span8">
                    <h5><a href="https://www.facebook.com/dozerabbasi">MD Salman Hekim</a></h5>
                    <p>MBA<br> <a href="http://www.lus.ac.bd/">Leading University</a><br>
                        Mobile: N\A</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix small-gap"></div>
        <div class="span3 no-margin">
            <div class="box director-home">
                <div class="pull-left span3"><p><img src="https://graph.facebook.com/makhan007/picture?type=square" alt="Mahfuz Khan" ></p></div>
                <div class="pull-right span8">
                    <h5><a href="https://plus.google.com/u/0/+MahfuzKhan001">Mahfuz A. Khan</a></h5>
                    <p>Naval Architecture<br> <a href="http://www.buet.ac.bd/">BUET</a><br>
                        Mobile: 01737758344</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="span3">
            <div class="box director-home">
                <div class="pull-left span3"><p><img src="https://graph.facebook.com/tanzimmubarrat/picture?type=square" alt="Tanzim" ></p></div>
                <div class="pull-right span8">
                    <h5><a href="https://www.facebook.com/tanzimmubarrat">Syed Tanzim Mubarrat</a></h5>
                    <p>EEE<br> <a href="http://www.buet.ac.bd/">BUET</a><br>
                        Mobile: 01715397179</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="span3">
            <div class="box director-home">
                <div class="pull-left span3"><p><img src="https://graph.facebook.com/muhib724/picture?type=square" alt="Muhib" ></p></div>
                <div class="pull-right span8">
                    <h5><a href="https://www.facebook.com/muhib724">Muhib Rahman</a></h5>
                    <p>Naval Architecture<br> <a href="http://www.buet.ac.bd/">BUET</a><br>
                        Mobile: 01755410724</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="span3">
            <div class="box director-home">
                <div class="pull-left span3"><p><img src="https://graph.facebook.com/boldman.jahed/picture?type=square" alt="Mahbub" ></p></div>
                <div class="pull-right span8">
                    <h5><a href="https://www.facebook.com/boldman.jahed">Jahid Ahmed Jahed</a></h5>
                    <p>Manager<br><br>
                        Mobile: N\A</p>
                    <!--
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix small-gap"></div>
        
        <div class="clearfix small-gap"></div>
    </div>
</div>